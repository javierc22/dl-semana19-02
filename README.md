# Semana 19 - Ejemplo 2: Carro de compras

### Relaciones N a N y Carro de compras

1. Intro a relaciones N a N
2. Modelos y relaciones
3. Agregando datos
4. Borrando registros y asociaciones
5. Index de peliculas
6. Agregando Tags
7. Verificando el Post
8. Borrado de Tags
9. Verificando el borrado
10. Carro de compras
11. Creando un proyecto con postgresql
12. Instalando Devise
13. Modelo y controller de producto
14. Faker y Seed
15. Vista de productos
16. Modelo de orden de compra
17. Link y ruta anidada para la compra
18. Procesando la orden de compra
19. Listado de ordenes

Crear proyecto con postgresql
~~~
$ rails new project_name -d postgresql
$ rails db:create
~~~

Gema Faker

https://github.com/stympy/faker

## Historia de usuario 1

Un usuario entra a la vista de productos y desde ahí genera
órdenes de compra de los diversos productos, para luego
poder pagar por todos los elementos seleccionados.

## Historia de usuario 2

Un usuario puede ver todas las órdenes de compra no
pagadas, y ver el precio que tiene que pagar para luego
poder generar efectivamente la compra.

![](modelo_conceptual.png)

### Modelo Lógico
![](modelo_logico.png)